######In this file, we do the feature engineering by multiply DebtRatio with MonthlyIncome to get DebtAmt. Then drop MonthlyIncome and
#####DebtRatio.

setwd('/Users/Arsenal4ever/Desktop/ADA/Project/Main')
training = read.csv('./cs-trainig-outlier.csv', header =T)
test = read.csv('./cs-testing-outlier.csv', header = T)
names(training)


DebtAmt_training = training$DebtRatio * as.numeric(training$MonthlyIncome)
DebtAmt_test = test$DebtRatio * as.numeric(test$MonthlyIncome)

training = as.data.frame(cbind(training, DebtAmt_training))
training = training[,-c(6,7)]

test = as.data.frame(cbind(test, DebtAmt_test))
test = test[,-c(6,7)]

write.csv(training,'training-debt.csv')
write.csv(test,'test-debt.csv')
