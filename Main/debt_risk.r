setwd('/Users/Arsenal4ever/Desktop/ADA/Project/Main')
training = read.csv('./cs-trainig-outlier.csv', header =T)
test = read.csv('./cs-testing-outlier.csv', header = T)
names(training)
library(dplyr)

new = training %>% filter(MonthlyIncome != 0)%>%group_by(NumberOfDependents) %>% summarise(avg=median(MonthlyIncome,na.rm=T))
names(new)

DepdRisk = 1:dim(training)[1]
for(i in 1:dim(training)[1]){
        if(is.na(training$MonthlyIncome[i])){
                DepdRisk[i] = NA
        }else{
                if(training$MonthlyIncome[i] == 0){
                        DepdRisk[i] = training$NumberOfDependents[i]/(new[new$NumberOfDependents==training$NumberOfDependents[i],]$avg)
                }else{
                        DepdRisk[i] = training$NumberOfDependents[i]/as.numeric(training$MonthlyIncome[i])
                }
        }
}

training = as.data.frame(cbind(training, DepdRisk))

training = training[,-c(7,12)]

write.csv(training,'training-depdrisk.csv')


new_1 = test %>% filter(MonthlyIncome != 0)%>%group_by(NumberOfDependents) %>% summarise(avg=median(MonthlyIncome,na.rm=T))
names(new)
DepdRisk = 1:101503
for(i in 1:101503){
        if(is.na(test$MonthlyIncome[i])){
                DepdRisk[i] = NA
        }else{
                if(test$MonthlyIncome[i] == 0){
                        DepdRisk[i] = test$NumberOfDependents[i]/(new[new$NumberOfDependents==test$NumberOfDependents[i],]$avg)
                }else{
                        DepdRisk[i] = test$NumberOfDependents[i]/as.numeric(test$MonthlyIncome[i])
                }
        }
}
test = as.data.frame(cbind(test, DepdRisk))

test = test[,-c(7,12)]

write.csv(test,'test-debtrisk.csv')
