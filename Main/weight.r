setwd('/Users/Arsenal4ever/Desktop/ADA/Project/Main')
training = read.csv('./cs-trainig-outlier.csv', header =T)
test = read.csv('./cs-testing-outlier.csv', header = T)

require(rms)

logit.x1 = lrm(SeriousDlqin2yrs ~ NumberOfTime3059DaysPastDueNotWorse,data = training)
logit.x1


logit.x2 = lrm(SeriousDlqin2yrs ~ NumberOfTime6089DaysPastDueNotWorse,data = training)
logit.x2

logit.x3 = lrm(SeriousDlqin2yrs ~ NumberOfTimes90DaysLate,data = training)
logit.x3

r=c(0.105,0.089,0.119)

sum = sum(r)

w1 = r[1]/sum
w2 = r[2]/sum
w3 = r[3]/sum

default_time = w1 * training$NumberOfTime3059DaysPastDueNotWorse +w2 * training$NumberOfTime6089DaysPastDueNotWorse + w3 * training$NumberOfTimes90DaysLate
training = as.data.frame(cbind(training,default_time))
training = training[,-c(5,9,11)]
write.csv(training,'training-weight.csv')

default_time = w1 * test$NumberOfTime3059DaysPastDueNotWorse +w2 * test$NumberOfTime6089DaysPastDueNotWorse + w3 * test$NumberOfTimes90DaysLate
test = as.data.frame(cbind(test,default_time))
test = test[,-c(5,9,11)]
write.csv(test,'test-weight.csv')