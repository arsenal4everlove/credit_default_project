# training data
setwd('/Users/Arsenal4ever/Desktop/ADA/Project/Main')
training.weight <- read.csv("~/Desktop/ADA/Project/Main/training_data/training-weight.csv")
training.depdrisk <- read.csv("~/Desktop/ADA/Project/Main/training_data/training-depdrisk.csv")
training.debt <- read.csv("~/Desktop/ADA/Project/Main/training_data/training-debt.csv")
training.agerisk <- read.csv("~/Desktop/ADA/Project/Main/training_data/training-agerisk.csv")

training.combine = training.weight[,-c(1,2,5,6,7,10)]
training.combine  = cbind(training.combine, training.agerisk$AgeRisk_training,training.debt$DebtAmt_training,training.depdrisk$DepdRisk)

write.csv(training.combine, './training_data/training.combine.csv')

# testing data
test.debt <- read.csv("~/Desktop/ADA/Project/Main/testin_data/test-debt.csv")
test.weight <- read.csv("~/Desktop/ADA/Project/Main/testin_data/test-weight.csv")
test.debtrisk <- read.csv("~/Desktop/ADA/Project/Main/testin_data/test-debtrisk.csv")
test.agerisk <- read.csv("~/Desktop/ADA/Project/Main/testin_data/test-agerisk.csv")

test.combine = test.weight[,c(2,3,7,8,10)]
test.combine  = cbind(test.combine, test.agerisk$AgeRisk_testing,test.debt$DebtAmt_test,test.debtrisk$DepdRisk)
write.csv(test.combine,'./testin_data/test.combine.csv')
