setwd('/Users/Arsenal4ever/Desktop/ADA/Project/Main')
training = read.csv('./cs-trainig-outlier.csv', header =T)
test = read.csv('./cs-testing-outlier.csv', header = T)
names(training)
summary(training)

##By the summary, we can apply log transformation on RevolvingUtilizationOfUnsecuredLines,NumberOfTime30.59DaysPastDueNotWorse,
##DebtRatio, MonthlyIncome, NumberOfOpenCreditLinesAndLoans, NumberOfTimes90DaysLate, NumberRealEstateLoansOrLines, NumberOfTime60.89DaysPastDueNotWorse
##NumberOfDependents

training$RevolvingUtilizationOfUnsecuredLines = log(training$RevolvingUtilizationOfUnsecuredLines+1)
training$NumberOfTime3059DaysPastDueNotWorse = log(training$NumberOfTime3059DaysPastDueNotWorse+1)
training$DebtRatio = log(training$DebtRatio+1)
training$MonthlyIncome = log(training$MonthlyIncome+1)
training$NumberOfOpenCreditLinesAndLoans = log(training$NumberOfOpenCreditLinesAndLoans+1)
training$NumberOfTimes90DaysLate = log(training$NumberOfTimes90DaysLate+1)
training$NumberOfTime6089DaysPastDueNotWorse = log(training$NumberOfTime6089DaysPastDueNotWorse+1)
training$NumberOfDependents = log(training$NumberOfDependents+1)
training$NumberRealEstateLoansOrLines = log(training$NumberRealEstateLoansOrLines+1)
write.csv(training,'cs-training-log.csv')

summary(test)
test$RevolvingUtilizationOfUnsecuredLines = log(test$RevolvingUtilizationOfUnsecuredLines+1)
test$NumberOfTime3059DaysPastDueNotWorse = log(test$NumberOfTime3059DaysPastDueNotWorse+1)
test$DebtRatio = log(test$DebtRatio+1)
test$MonthlyIncome = log(test$MonthlyIncome+1)
test$NumberOfOpenCreditLinesAndLoans = log(test$NumberOfOpenCreditLinesAndLoans+1)
test$NumberOfTimes90DaysLate = log(test$NumberOfTimes90DaysLate+1)
test$NumberOfTime6089DaysPastDueNotWorse = log(test$NumberOfTime6089DaysPastDueNotWorse+1)
test$NumberOfDependents = log(test$NumberOfDependents+1)
test$NumberRealEstateLoansOrLines = log(test$NumberRealEstateLoansOrLines+1)

write.csv(test,'cs-test-log.csv')