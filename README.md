# Credit Default Project

Our project focuses on working on a credit score algorithm that can be used to predict default probability based on individual’s personal and credit information. The main challenge comes from limit of personal credit information (no time-varying credit information available) and traditional survival analysis on credit default can’t be used. We design a two layer stacking method to get a good generalization model and ranked at top 10% on kaggle.

This project helps us be familiar with the workflow of using machine learning to solve real life problem. It also gives us a taste of dealing with financial data.